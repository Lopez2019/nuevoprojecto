-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.1.72-community - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para grupos_iuc
DROP DATABASE IF EXISTS `grupos_iuc`;
CREATE DATABASE IF NOT EXISTS `grupos_iuc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `grupos_iuc`;

-- Volcando estructura para tabla grupos_iuc.estudiantes
DROP TABLE IF EXISTS `estudiantes`;
CREATE TABLE IF NOT EXISTS `estudiantes` (
  `idestudiantes` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `edad` varchar(50) DEFAULT NULL,
  `tipo_documento` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `grupos_idgrupos` int(11) NOT NULL,
  PRIMARY KEY (`idestudiantes`),
  KEY `FK_estudiantes_grupos` (`grupos_idgrupos`),
  CONSTRAINT `FK_estudiantes_grupos` FOREIGN KEY (`grupos_idgrupos`) REFERENCES `grupos` (`idgrupos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla grupos_iuc.estudiantes: ~14 rows (aproximadamente)
DELETE FROM `estudiantes`;
/*!40000 ALTER TABLE `estudiantes` DISABLE KEYS */;
INSERT INTO `estudiantes` (`idestudiantes`, `nombre`, `apellidos`, `edad`, `tipo_documento`, `genero`, `grupos_idgrupos`) VALUES
	(1002356784, 'juan', 'rendon', '16', 'ID', 'masculino', 3),
	(1002456678, 'allison', 'cardona', '16', 'ID', 'femenino', 1),
	(1002456783, 'jose', 'gonzales', '15', 'ID', 'masculino', 1),
	(1002786432, 'camila', 'blanco', '17', 'ID', 'femenino', 2),
	(1002899551, 'yeison', 'castro', '18', 'CD', 'masculino', 1),
	(1003059567, 'thomas', 'turbado', '15', 'ID', 'femenino', 1),
	(1003267865, 'nicolas', 'gonzales', '17', 'ID', 'masculino', 2),
	(1003456789, 'robinson', 'escobar', '17', 'ID', 'masculino', 1),
	(1003467529, 'carlos', 'santa', '17', 'ID', 'femenino', 2),
	(1003567894, 'valentina ', 'salazar', '15', 'ID', 'femenino', 3),
	(1003981239, 'natalia', 'rodriguez', '16', 'ID', 'femenino', 3),
	(1004321895, 'ana', 'sanchez', '15', 'ID', 'femenino', 3),
	(1004587123, 'manuela', 'rodriguez', '17', 'ID', 'femenino', 3),
	(1006742396, 'carlos', 'gomez', '17', 'ID', 'masculino', 3);
/*!40000 ALTER TABLE `estudiantes` ENABLE KEYS */;

-- Volcando estructura para tabla grupos_iuc.grupos
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE IF NOT EXISTS `grupos` (
  `idgrupos` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `numero_estudiantes` int(11) NOT NULL,
  `aula` int(11) NOT NULL,
  PRIMARY KEY (`idgrupos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla grupos_iuc.grupos: ~3 rows (aproximadamente)
DELETE FROM `grupos`;
/*!40000 ALTER TABLE `grupos` DISABLE KEYS */;
INSERT INTO `grupos` (`idgrupos`, `nombre`, `numero_estudiantes`, `aula`) VALUES
	(1, 111, 5, 505),
	(2, 112, 3, 506),
	(3, 113, 6, 507);
/*!40000 ALTER TABLE `grupos` ENABLE KEYS */;

-- Volcando estructura para tabla grupos_iuc.maestros
DROP TABLE IF EXISTS `maestros`;
CREATE TABLE IF NOT EXISTS `maestros` (
  `idmaestros` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `documento` varchar(50) DEFAULT NULL,
  `grupos_idgrupos` int(11) NOT NULL,
  PRIMARY KEY (`idmaestros`),
  KEY `FK_maestros_grupos` (`grupos_idgrupos`),
  CONSTRAINT `FK_maestros_grupos` FOREIGN KEY (`grupos_idgrupos`) REFERENCES `grupos` (`idgrupos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla grupos_iuc.maestros: ~3 rows (aproximadamente)
DELETE FROM `maestros`;
/*!40000 ALTER TABLE `maestros` DISABLE KEYS */;
INSERT INTO `maestros` (`idmaestros`, `nombre`, `apellidos`, `titulo`, `celular`, `documento`, `grupos_idgrupos`) VALUES
	(25108005, 'sofia', 'visbal', 'ed fisica', NULL, NULL, 1),
	(25677889, 'lisa', 'escobar', 'ingles', NULL, NULL, 3),
	(30987654, 'maria olga\r\n', 'osorio', 'sociales', NULL, NULL, 2);
/*!40000 ALTER TABLE `maestros` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
